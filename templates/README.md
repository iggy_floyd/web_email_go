# templates
--
    import "."

@ Igor Marfin <Unister Gmb, 2014> igor.marfin@unister.de

About templates ...


### References

Here is a list of open publications used to prepare this package:

## Usage

#### type Page

```go
type Page struct {
	Title  string
	Body   string
	Alerts []string
}
```

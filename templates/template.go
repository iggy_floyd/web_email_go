
package templates



import (
    "net/http"
//    "fmt"
    "github.com/gorilla/mux"
    "html/template"
    "path"
    "os"
    "log"
//    "sync"
)

type Page struct {
	Title  string
	Body   string
	Alerts []string
}


// a router
var router = mux.NewRouter()

var p *Page
var templatename string

//var (
//    templates = template.Must(template.ParseFiles("index.tmpl"))
//)

// initialization
//func init() {
func Init() {
    log.Println("templates example starting...")
    p  = &Page{"Test", "Test" ,[]string{"Hi","It is me"}}    
    templatename = "index"
/*
//    router.Handle("/",handler(renderTemplate))
    router.HandleFunc("/",func(w http.ResponseWriter, r *http.Request) {
               renderTemplate(w, r, templatename,p)

    })
    http.ListenAndServe(":8200", router)
*/


    http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
                renderTemplate(w, r, templatename,p)    })
 
    http.ListenAndServe(":8200", nil)

}


type handler func(w http.ResponseWriter, r *http.Request,tmpl string,p *Page)


func (h handler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
      h(w,r,templatename,p)
}



func renderTemplate(w http.ResponseWriter, r *http.Request, tmpl string, p *Page) {
        lp := path.Join("templates/layout", "layout.tmpl")
        fp := path.Join("templates/layout", r.URL.Path+".tmpl")
//        fmt.Println(fp)
//        fmt.Println(lp)
//        templates, _ := template.ParseFiles(lp, fp)

// Return a 404 if the template doesn't exist
  info, err := os.Stat(fp)
  if err != nil {
    if os.IsNotExist(err) {
      log.Println(err.Error())
      http.NotFound(w, r)
      return
    }
  }

  // Return a 404 if the request is for a directory
  if info.IsDir() {
    http.NotFound(w, r)
    return
  }
//        templates :=  template.Must(template.ParseFiles(lp, fp))
  templates, err := template.ParseFiles(lp, fp)
  if err != nil {
    // Log the detailed error
    log.Println(err.Error())
    // Return a generic "Internal Server Error" message
    http.Error(w, http.StatusText(500), 500)
    return
  }
        
//	err := templates.ExecuteTemplate(w, tmpl+".tmpl", p)
	err = templates.ExecuteTemplate(w, "layout", p)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}

}

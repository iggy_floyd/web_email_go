
package view



import (
    "net/http"
    "html/template"
    "path"
    "os"
    "log"
)

type Page struct {
	Title  string
	Body   string
	Footer template.HTML
        Session string
}


var p *Page




func RenderTemplate(w http.ResponseWriter, r *http.Request,  p *Page) {
        lp := path.Join("view/templates", "layout.tmpl")
        fp := path.Join("view/templates", r.URL.Path+".tmpl")

// Return a 404 if the template doesn't exist
  info, err := os.Stat(fp)
  if err != nil {
    if os.IsNotExist(err) {
      log.Println(err.Error())
    fp = path.Join("view/templates", "home.tmpl")    
    info,_ = os.Stat(fp)
    }
  }

  // Return a 404 if the request is for a directory
  if info.IsDir() {
    http.NotFound(w, r)
    return
  }

  templates, err := template.ParseFiles(lp, fp)
  if err != nil {
    // Log the detailed error
    log.Println(err.Error())
    // Return a generic "Internal Server Error" message
    http.Error(w, http.StatusText(500), 500)
    return
  }
        
   err = templates.ExecuteTemplate(w, "layout", p)
   if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
   }

}

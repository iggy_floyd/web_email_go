package controller

import (
    "net/http"
    "fmt"
    "github.com/gorilla/sessions"
    "log"
     
)

// session storage: a Cookie
var (
  store = sessions.NewCookieStore([]byte("something-very-secret"))
  sessioname = "_qwwqsession"
)

// initialization
func init() {
    log.Println("session started...")
    store.Options = &sessions.Options{
        Path:     "/",
        MaxAge:   3600 * 1, // 1 hour
        HttpOnly: true,
    }
}

func GetSession(r *http.Request) (session *sessions.Session,err error) {
  return store.Get(r, sessioname)

}

func CheckSession(r *http.Request) bool {
   session,_:= store.Get(r, sessioname)
//   fmt.Println("1234")
//   fmt.Println(session.Values)
//   fmt.Println("r44565")
   if _,ok := session.Values["email"]; ok { 
    return true
 } 
 return false
}


func SetSession(w http.ResponseWriter,r *http.Request,  m map[interface{}]interface{}) {
   session,_:= store.Get(r, sessioname)
   session.Values = m
   err := session.Save(r, w)
   if err != nil {
              log.Fatal("Session not saved!", err)
              w.Write([]byte(fmt.Sprintln("Session is not saved! ", err)))
   }

}

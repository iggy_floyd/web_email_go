package controller

import (
//   "fmt"
   "net/http"    
//   "html/template"
//   "path"
//   . "../view"
   "log"
   . "../model"
)


//handler for signIn
func SignInHandler(w http.ResponseWriter, r *http.Request) {

    if r.Method == "GET" {
     http.Redirect(w, r, "/signin", http.StatusFound)
    } else {
      r.ParseForm()
//      email := r.Form["email"]
//      password := r.Form["password"]
//      fmt.Println(email)
//      session,_ := GetSession(r)

      if !CheckSession(r) {
        m := make(map[interface{}]interface{})
        m["email"] =   r.Form["email"]
        u := User{r.Form["email"][0],r.Form["password"][0]}
        if u.CheckUser() {
         SetSession(w,r,m)
        }
        log.Println("Session is invalid")
        http.Redirect(w, r, "/", http.StatusFound)
      } else {
        log.Println("Session is valid")
        http.Redirect(w, r, "/", http.StatusFound)
      }


    }

}

//handler for logout
func LogOutHandler(w http.ResponseWriter, r *http.Request) {
    session,_ := GetSession(r)
    if CheckSession(r) {
      delete(session.Values, "email")
      session.Save(r, w)
    }    
    http.Redirect(w, r, "/login", http.StatusFound)
}


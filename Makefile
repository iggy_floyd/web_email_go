
# @ Igor Marfin <Unister Gmb, 2014> igor.marfin@unister.de
# simple makefile to manage this project


description = 'Application Monte-Carlo techniques in Option Trading'
SHELL = /bin/bash


all: configuration  README.md


# to check that the system has all needed components
configuration: configure
	@./configure


# to check an update in the configure.ac. If it's found, update the 'configure' script.
configure: configure.ac
	@./configure.ac


#doc: README.rst
#	-@mkdir doc 2>/dev/null
##	-@rst-tool/create_docs.sh README.rst `basename $(PWD)` $(description); mv README.html doc;
#	-@rst2html.py README.md >doc/index.html;
#	-@ln -s $(PWD)/images/ doc/

README.md: doc.go
	-@export GOROOT=$(GOROOT); export GOPATH=$(GOPATH); find `pwd` -iname "doc*go" |  xargs -I {} echo "cd \`dirname {}\`; $(GOPATH)/bin/godocdown  -output=README.md" | bash

test:
	-@echo "Test: Not implemented"


run: configuration test
	-@echo "Run: Not implemented"


build: configuration test
	-@echo "Build: Not implemented"


# to clean all temporary stuff
clean: 
	-@rm -r config.log autom4te.cache
	-@rm -r doc/ public
	-@rm README


#serve:
#	-@firefox localhost:8010 &
#	-@ln -s doc/ public
#	-@pushd public/;    python -m SimpleHTTPServer 8010; popd;



.PHONY: configuration clean all doc run test build


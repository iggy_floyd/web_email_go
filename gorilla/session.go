package gorilla

import (
    "net/http"
    "fmt"
    "github.com/gorilla/sessions"
    "github.com/gorilla/mux"
    "log"
     
)

// session storage: a Cookie
var store = sessions.NewCookieStore([]byte("something-very-secret"))

// a router
var router = mux.NewRouter()


// initialization
//func init() {
func Init() {
    log.Println("session example starting...")
    store.Options = &sessions.Options{
//        Domain:   "localhost",
        Path:     "/",
        MaxAge:   3600 * 1, // 1 hour
        HttpOnly: true,
    }

// check the ways to find cookies stored at your machine using firefox
//  http://superuser.com/questions/392176/view-cookies-in-mozilla-firefox
    router.HandleFunc("/",SetSessionHandler)
    router.HandleFunc("/getsession",GetSessionHandler)
    log.Fatal(http.ListenAndServe(":8100", router))
}



// SetHandler to serve http.Request and Set Cookies (sessions)
func SetSessionHandler(w http.ResponseWriter, r *http.Request) {
    // Get a session. We're ignoring the error resulted from decoding an
    // existing session: Get() always returns a session, even if empty.
    session, err := store.Get(r, "session-name")
    if err != nil {
        http.Error(w, err.Error(), http.StatusInternalServerError)
        return
    }

    // Set some session values.
    session.Values["foo"] = "bar"
    session.Values[42] = 43

    // Save it before we write to the response/return from the handler.
    err = session.Save(r, w)
    if err != nil {
                 fmt.Println("Session not saved!", err)
                 w.Write([]byte(fmt.Sprintln("Setting not saved! ", err)))
    }   
    fmt.Fprint(w, "Hello world :)")
}


// GetHandler to serve http.Request and Set Cookies (sessions)
func GetSessionHandler(w http.ResponseWriter, r *http.Request) {

    // reading session values - without adding it to registry
    //  session, err := store.New(r, "session-name")
    session, err := store.Get(r, "session-name")
    if err != nil {
                 fmt.Println("Unable to retrieve session!", err)
                 w.Write([]byte(fmt.Sprintln("Unable to retrieve session! ", err)))
    }
    fmt.Println("Session name is : ", session.Name())
    fmt.Println("Session values from SetSessionHandler...")
    for k, v := range session.Values {
                 fmt.Printf("Key : [%v] Values : [%v]\n", k, v)
    }
    // ONLY write to client after session is read.
    w.Write([]byte(session.Name()))
    w.Write([]byte(fmt.Sprintf("\nGetting session...\n")))
    w.Write([]byte(fmt.Sprintln(session.Values)))
    
}

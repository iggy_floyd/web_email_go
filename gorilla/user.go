

package gorilla

import (
    "log"
    "github.com/boltdb/bolt"
    "fmt"
    "encoding/json"
)

// a user record
type User struct {
    FirstName string
    FamilyName string
    ID int64
}


// IUser interface
type IUser interface {
    userInfo() []byte
    save2DB() error
    readFromDB() error
}



// print a user info
func (u User) userInfo() []byte {

 b, err := json.Marshal(u)
 if err != nil {
    fmt.Println("A user can't be shown!", err)
 }
 return b
}


// open connection to  the DB
func openDB() (*bolt.DB,error){
 db, err := bolt.Open("data.db", 0600, nil)
  if err != nil {
        log.Fatal(err)
        return nil,err
    }
  return db,nil
}


// save a user to the DB
//http://www.progville.com/go/bolt-embedded-db-golang/
func (u User) save2DB() error {
    db, err := openDB()
    if err != nil {
       return err
    }
    defer db.Close()

   err =  db.Update(func(tx *bolt.Tx) error {

    // userkey in the DB
    userkey := u.FirstName + u.FamilyName
    var key = make([]byte,len(userkey))
    copy(key[:], userkey)
    // a new bucket: "users"
    b, err := tx.CreateBucketIfNotExists([]byte("users"))
     if err != nil {
        return err
    }
   return b.Put(key,u.userInfo())
   }) // func(tx *bolt.Tx) error
   if err != nil {
        log.Fatal(err)
        return err
   }

   return nil
}


// get a user from the DB
//http://www.progville.com/go/bolt-embedded-db-golang/
func (u *User) readFromDB() error {
  db, err := openDB()
  if err != nil {
      return err
    }
  defer db.Close()
  err = db.View(func(tx *bolt.Tx) error {
    // userkey in the DB
    userkey := u.FirstName + u.FamilyName
    var key = make([]byte,len(userkey))
    copy(key[:], userkey)

    bucket := tx.Bucket([]byte("users"))
    if bucket == nil {
            return fmt.Errorf("Bucket %q not found!", []byte("users"))
      }
    val := bucket.Get(key)
    err = json.Unmarshal(val, u)    
    if err != nil {
        return err
    }
   return nil
  }) // func(tx *bolt.Tx) error
  if err != nil {
        log.Fatal(err)
        return err
   }
   return nil
}



func test(){

  // create a user      
  u := User{"Igor","Marfin",1}
  fmt.Println(string(u.userInfo()))
  // save to the DB
  u.save2DB() 

  // create a user with unknown ID
  u2 := User{"Igor","Marfin",0}
  fmt.Println(string(u2.userInfo()))
  // read it from DB
  u2.readFromDB()
  fmt.Println(string(u2.userInfo())) 
}

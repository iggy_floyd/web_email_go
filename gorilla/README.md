# gorilla
--
    import "."

@ Igor Marfin <Unister Gmb, 2014> igor.marfin@unister.de

Gorilla is a web toolkit for the Go programming language. Currently these
packages are available:

    gorilla/context stores global request variables.
    gorilla/mux is a powerful URL router and dispatcher.
    gorilla/reverse produces reversible regular expressions for regexp-based muxes.
    gorilla/rpc implements RPC over HTTP with codec for JSON-RPC.
    gorilla/schema converts form values to a struct.
    gorilla/securecookie encodes and decodes authenticated and optionally encrypted cookie values.
    gorilla/sessions saves cookie and filesystem sessions and allows custom session backends.
    gorilla/websocket implements the WebSocket protocol defined in RFC 6455.


### References

Here is a list of open publications used to prepare this package:

## Usage

#### func  GetSessionHandler

```go
func GetSessionHandler(w http.ResponseWriter, r *http.Request)
```
GetHandler to serve http.Request and Set Cookies (sessions)

#### func  SetSessionHandler

```go
func SetSessionHandler(w http.ResponseWriter, r *http.Request)
```
SetHandler to serve http.Request and Set Cookies (sessions)

#### type IUser

```go
type IUser interface {
	// contains filtered or unexported methods
}
```

IUser interface

#### type User

```go
type User struct {
	FirstName  string
	FamilyName string
	ID         int64
}
```

a user record

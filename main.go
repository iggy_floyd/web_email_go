package main

import (
//	_ "./gorilla"
//	_ "./templates"
	 "./gorilla"
	 "./templates"
	 "./view"
         "sync"        
)

//session handling
func main() {

 wg := &sync.WaitGroup{}
	wg.Add(1)
	go func() {
		gorilla.Init()
		wg.Done()
	}()
	wg.Add(1)
	go func() {
		templates.Init()
		wg.Done()
	}()
	wg.Add(1)
	go func() {
		view.Init()
		wg.Done()
	}()
	wg.Wait()

}

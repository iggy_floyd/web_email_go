// @ Igor Marfin <Unister Gmb, 2014> igor.marfin@unister.de
//
// Introduction
// 
// web-mail-go is a simple package illustrating basic web eamil service realized wuthu means of GOlang.
//
//
// You can clone the git repo
//
//     git clone https://iggy_floyd@bitbucket.org/iggy_floyd/web_email_go.git
//
// Afterwards, compile and execute the binary
//
//    cd web_email_go
//    make
//
//
//
// Then you can open documentation in the browser:
//
//   go get code.google.com/p/go.tools/cmd/godoc
//   go get -u bitbucket.org/iggy_floyd/virtual_machine_python/web_email_go  #(this repo is made to be public, to allow such "go-getting" )
//   godoc -http=:8080
//   firefox http://localhost:8080/pkg/bitbucket.org/iggy_floyd/web_email_go
// 
// References
//
// Here is a list of open publications used to prepare this package:
//
//
//
package main

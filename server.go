package main

import ( 
	. "./view"
        . "./controller"
          "log"
          "net/http"
          "html/template"
          "strings"
)

var p *Page



func main() {

  p  = &Page{
		Title: "Test", 
		Body: "Test" ,
		Footer: template.HTML("&copy; 2016 Igor Marfin"),
	}

   fs := http.FileServer(http.Dir("view/static"))
   http.Handle("/static/", http.StripPrefix("/static/", fs))

   http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
       if !CheckSession(r) && !strings.Contains(r.URL.Path, "signin"){
	 http.Redirect(w, r, "/login", http.StatusFound)
       }
       if CheckSession(r) {
          session,_ := GetSession(r)
          p.Session = session.Values["email"].([]string)[0]
       } else {
          p.Session = ""
       }
       RenderTemplate(w, r,p)    
   })
   http.HandleFunc("/login",SignInHandler)
   http.HandleFunc("/logout",LogOutHandler)

   log.Println("Server is starting...")
   http.ListenAndServe(":8300", nil)

}

package model

import (
    "log"
    "github.com/boltdb/bolt"
    "fmt"
    "encoding/json"
    "crypto/md5"
    "encoding/hex"
)

//"test1234"
var secret = "16d7a4fca7442dda3ad93c9a726597e4"

func init() {
  // create a user
  u := User{"test@example.com","test1234"}
  // save to the DB
  u.Save2DB()
}

// a user record
type User struct {
    Email string
    Passwd string
}


// IUser interface
type IUser interface {
    UserInfo() []byte
    Save2DB() error
    ReadFromDB() error
    CheckUser() bool
}

func (u *User) CheckUser() bool {
  log.Println(GetMD5Hash(u.Passwd))
  log.Println(secret)
  return GetMD5Hash(u.Passwd) == secret
}


// print a user info
func (u User) UserInfo() []byte {

 b, err := json.Marshal(u)
 if err != nil {
    fmt.Println("A user can't be shown!", err)
 }
 return b
}


// open connection to  the DB
func openDB() (*bolt.DB,error){
 db, err := bolt.Open("data.db", 0600, nil)
  if err != nil {
        log.Fatal(err)
        return nil,err
    }
  return db,nil
}


// save a user to the DB
//http://www.progville.com/go/bolt-embedded-db-golang/
func (u User) Save2DB() error {
    db, err := openDB()
    if err != nil {
       return err
    }
    defer db.Close()

   err =  db.Update(func(tx *bolt.Tx) error {

    // userkey in the DB
    userkey := u.Email
    var key = make([]byte,len(userkey))
    copy(key[:], userkey)
    // a new bucket: "users"
    b, err := tx.CreateBucketIfNotExists([]byte("users"))
     if err != nil {
        return err
    }
   return b.Put(key,u.UserInfo())
   }) // func(tx *bolt.Tx) error
   if err != nil {
        log.Fatal(err)
        return err
   }

   return nil
}


// get a user from the DB
//http://www.progville.com/go/bolt-embedded-db-golang/
func (u *User) ReadFromDB() error {
  db, err := openDB()
  if err != nil {
      return err
    }
  defer db.Close()
  err = db.View(func(tx *bolt.Tx) error {
    // userkey in the DB
    userkey := u.Email
    var key = make([]byte,len(userkey))
    copy(key[:], userkey)

    bucket := tx.Bucket([]byte("users"))
    if bucket == nil {
            return fmt.Errorf("Bucket %q not found!", []byte("users"))
      }
    val := bucket.Get(key)
    err = json.Unmarshal(val, u)    
    if err != nil {
        return err
    }
   return nil
  }) // func(tx *bolt.Tx) error
  if err != nil {
//        log.Fatal(err)
     return err
   }
   return nil
}

func GetMD5Hash(text string) string {
    hasher := md5.New()
    hasher.Write([]byte(text))
    return hex.EncodeToString(hasher.Sum(nil))
}

